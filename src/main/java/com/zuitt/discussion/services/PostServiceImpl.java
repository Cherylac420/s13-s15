// contains the business logic concerned with a particular object in the class
package com.zuitt.discussion.services;


import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
// Will allow us to use the CRUD repository method inherited from the CRUDRepository
@Service
public class PostServiceImpl implements PostService{
    // An object cannot be instantiated from interfaces.
    // @Autowired allows us to use the interface as if it was an instance of an object and allows us to use the methods from the CRUDRespository
    @Autowired
    private PostRepository postRepository;

   @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // Create post
    public void createPost(String stringToken, Post post){
        //Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }
    // Create post
    public void createPost(Post post){
        postRepository.save(post);
    }
    // Get all post
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    // Delete post
    public ResponseEntity deletePost(Long id){
       postRepository.deleteById(id);
       return new ResponseEntity<>("Post Deleted successful" +
               "ly", HttpStatus.OK);
    }

    // Update a post
    public ResponseEntity updatePost(Long id, Post post){
        // Find the post to update
        Post postForUpdate = postRepository.findById(id).get();
        // Updating the tittle and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());
        // Saving and Updating a post
        postRepository.save(postForUpdate);
        return new ResponseEntity<>("Post updated Successfully",HttpStatus.OK);
    }
}
